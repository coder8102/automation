package test.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import common.Suite;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PostTest extends Suite {

	private static final Logger logger = Logger.getRootLogger();
	
	@Test
	public void testStatusCode() {

		Map<String, String> bodyMap = new HashMap<String, String>();
		bodyMap.put("name", "morpheus");
		bodyMap.put("job", "leader");

		/*
		RestAssured.
			given().auth().basic(userName, password).contentType(ContentType.JSON).body(bodyMap).
			when().post("/api/users").
			then().statusCode(201);
	    */
		
		Response response = RestAssured.
			given().auth().basic(userName, password).contentType(ContentType.JSON).body(bodyMap).
			when().post("/api/users");
		
		response.then().statusCode(201);

		//conver response to json object
		JsonPath jsonPath = response.jsonPath();
		
		//set resourceId as id from response, use resourceId for testing PUT and DELETE action
		resourceId = jsonPath.getString("id");
		
		logger.info("Resource created through Post action with id: " + resourceId);
	}

}
