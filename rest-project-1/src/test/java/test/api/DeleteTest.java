package test.api;

import org.testng.annotations.Test;

import common.Suite;
import io.restassured.RestAssured;

public class DeleteTest extends Suite {

	@Test
	public void testStatusCode() {

		RestAssured.
			given().auth().basic(userName, password).
			when().delete("/api/users/" + resourceId).
			then().statusCode(204);
	}

}
