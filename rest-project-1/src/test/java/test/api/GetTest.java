package test.api;

import org.testng.annotations.Test;

import common.Suite;
import io.restassured.RestAssured;


public class GetTest extends Suite {

	@Test
    public void testStatusCode() {
		RestAssured.
			given().auth().basic(userName, password).
			when().get("/api/users").
			then().statusCode(200);
    }

}
