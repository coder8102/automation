package test.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import common.Suite;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PutTest extends Suite {

	@Test
	public void testStatusCode() {

		Map<String, String> bodyMap = new HashMap<String, String>();
		bodyMap.put("name", "morpheus");
		bodyMap.put("job", "zion resident");

		RestAssured.
			given().auth().basic(userName, password).contentType(ContentType.JSON).body(bodyMap).
			when().put("/api/users/" + resourceId).
			then().statusCode(200);
	}

}
