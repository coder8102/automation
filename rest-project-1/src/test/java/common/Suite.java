package common;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import io.restassured.RestAssured;


public class Suite {

	public String baseUrl = "https://reqres.in";
	public String userName = "TestUser";
	public String password = "password";
	
	private static final Logger logger = Logger.getRootLogger();
	
	public String resourceId;
	
	@BeforeSuite
	public void beforeSuite() {
		logger.debug("Inside beforeSuite");
	}
	
	@BeforeTest
	public void beforeTest() {
		logger.debug("Inside beforeTest");
	}
	
	@BeforeClass
	public void beforeClass() {
		logger.debug("Inside beforeClass");
		RestAssured.baseURI = baseUrl;
	}

	@BeforeMethod
	public void beforeMethod() {
		logger.debug("Inside beforeMethod");
	}
	
	@AfterMethod
	public void afterMethod() {
		logger.debug("Inside afterMethod");
	}
	
	@AfterClass(alwaysRun=true)
	public void afterClass() {
		logger.debug("Inside afterClass");
	}
	
	@AfterTest
	public void afterTest() {
		logger.debug("Inside afterTest");
	}
	
	@AfterSuite
	public void afterSuite() {
		logger.debug("Inside afterSuite");
	}
	
}
