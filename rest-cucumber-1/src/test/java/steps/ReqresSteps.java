package steps;

import org.apache.log4j.Logger;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class ReqresSteps {

	private static final Logger logger = Logger.getRootLogger();

	public String baseUrl = "https://reqres.in";
	public String userName = "TestUser";
	public String password = "password";

	private Response response;
	
    @Before
    public void setUp() {
    	logger.debug("setUp");
		RestAssured.baseURI = baseUrl;
    }

    @Given("^authentication parameters$")
    public void authentication_parameters() throws Throwable {
    	logger.debug("authentication_parameters");
		RestAssured.given().auth().basic(userName, password);
    }

    @When("^invoke users api$")
    public void invoke_users_api() throws Throwable {
    	logger.debug("invoke_users_api");
    	response = RestAssured.when().get("/api/users");
    }

    @Then("^return list of users$")
    public void list_of_users() throws Throwable {
    	logger.debug("list_of_users");
    	response.then().statusCode(200);
    }
}