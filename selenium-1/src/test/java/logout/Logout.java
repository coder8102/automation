package logout;

import org.openqa.selenium.support.PageFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import common.Suite;
import page.LoginPage;

public class Logout extends Suite {

	@Test
	public void success() {
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.signOn.click();
		loginPage.username.clear();
		loginPage.username.sendKeys("demo");
		loginPage.password.clear();
		loginPage.password.sendKeys("demo");
		loginPage.login.click();
		
		boolean isSignOffDisplayed = loginPage.signOff.isDisplayed();
		AssertJUnit.assertEquals(isSignOffDisplayed, true);

		loginPage.signOff.click();

		boolean isSignOnDisplayed = loginPage.signOn.isDisplayed();
		AssertJUnit.assertEquals(isSignOnDisplayed, true);
	}

}
