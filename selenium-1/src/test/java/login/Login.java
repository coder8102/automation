package login;

import org.openqa.selenium.support.PageFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import common.Suite;
import page.LoginPage;

public class Login extends Suite {

	@Test
	public void success() {
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.signOn.click();
		loginPage.username.clear();
		loginPage.username.sendKeys("demo");
		loginPage.password.clear();
		loginPage.password.sendKeys("demo");
		loginPage.login.click();

		boolean isSignOffDisplayed = loginPage.signOff.isDisplayed();
		AssertJUnit.assertEquals(isSignOffDisplayed, true);
	}

	@Test
	public void fail() {
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.signOn.click();
		loginPage.username.clear();
		loginPage.username.sendKeys("test");
		loginPage.password.clear();
		loginPage.password.sendKeys("test");
		loginPage.login.click();

		// catch NoSuchElementException for SIGN-OFF link and check if exist
		boolean isSignOffDisplayed = isElementExist(loginPage.bySignOff);
		AssertJUnit.assertEquals(isSignOffDisplayed, false);
	}

}
