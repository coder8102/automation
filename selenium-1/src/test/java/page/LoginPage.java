package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	@FindBy(name = "userName")
	public WebElement username;
	
	@FindBy(name = "password")
	public WebElement password;

	@FindBy(name = "login")
	public WebElement login;

	@FindBy(linkText = "SIGN-ON")
	public WebElement signOn;
	
	@FindBy(linkText = "SIGN-OFF")
	public WebElement signOff;
	
	public By bySignOff = By.linkText("SIGN-OFF");
}
