package common;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;


public class Suite {

	public WebDriver driver;
	public String baseUrl = "http://newtours.demoaut.com";
	
	private static final Logger logger = Logger.getRootLogger();
	
	@BeforeSuite
	public void beforeSuite() {
		logger.debug("Inside beforeSuite");
	}
	
	@BeforeTest
	public void beforeTest() {
		logger.debug("Inside beforeTest");
	}
	
	@BeforeClass
	public void beforeClass() {
		logger.debug("Inside beforeClass");

		System.setProperty("webdriver.chrome.driver", "C:/Selenium/chromedriver.exe");

		// need pageLoadStrategy to none to stop pageLoad wait for full page
		// load after clicking Login button
		ChromeOptions options = new ChromeOptions();
		options.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
		driver = new ChromeDriver(options);

		//driver.manage().window().maximize();

		// applicable to only findElement or findElements
		// if click on button takes more than configured implicit wait, it will still wait
		// click on login button loads page where PAGE_LOAD_STRATEGY set as none so will not wait for full page load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@BeforeMethod
	public void beforeMethod() {
		logger.debug("Inside beforeMethod");
		driver.get(baseUrl);
	}
	
	@AfterMethod
	public void afterMethod() {
		logger.debug("Inside afterMethod");
	}
	
	@AfterClass(alwaysRun=true)
	public void afterClass() {
		logger.debug("Inside afterClass");
		driver.quit();
		driver = null;
	}
	
	@AfterTest
	public void afterTest() {
		logger.debug("Inside afterTest");
	}
	
	@AfterSuite
	public void afterSuite() {
		logger.debug("Inside afterSuite");
	}
	
	public boolean isElementExist(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException nse) {
			return false;
		}
	}

}
